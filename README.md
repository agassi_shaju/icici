**Installation:**

Latest Version: **1.8.4**
**Changelog:**
Added webDriver.wait.pollingInterval in config.properties. You can make webdriver to poll for element every few ms
Added method to grab text from alert and assert (Check LoginSteps file)
New negative tests can be written like these
 @dailysession
    Scenario: Equity-AdvanceCashOrders Negative path (with no stock value) - Indian Customer
        Given I open app app.icicidirect
        # # # And If I see element landingPage.loginButton then I click element landingPage.loginButton
        And I login into icici direct portal as an Indian customer
        Then I can see text Equity
        When I click text Equity
        And I click text Place Order
        Then I can see text MTF Buy
        And I click text MTF Buy
        And I click element equity.PlaceOrder.bse
        #And I fill input equity.PlaceOrder.stock with value ADIFAS
        And I fill input equity.PlaceOrder.quantity with value 1
        And I click element equity.PlaceOrder.market
        And I click element equity.PlaceOrder.buyButton
        Then I check alert message The Stock field is empty. Please enter the Stock.
      

Clone the code using git clone repoUrl

Copy the latest version of nimbal platform from resources folder i.e auto-platform-1.8.4-tests.jar and drop it in .M2 folder
of maven with location .m2\repository\nz\co\nimbal\auto-platform\1.8.4

Go to root directory of the clone project and run `mvn install` command or run it using jenkins

Find the reports in target folder of the project 

**Remote Execution:**
_Login into tw-tree.firebaseapp.com portal and view live execution and recordings_

**To Upgrade your framework to new Nimbal auto release**

Pull the latest code from master - `git pull`

Check you have the platformjar in folder at location - resources/platformJars/

Open Terminal and run the following command by updating the jar location

mvn install:install-file -Dfile='/home/project/src/test/resources/1.8.5/auto-platform-1.8.5-tests.jar' -DgroupId='nz.co.nimbal' -DartifactId='auto-platform' -Dversion='1.8.5'  -Dpackaging='test-jar'

For any jar installation following are the values from fields (groupId,artifactId,version) you should capture for above command. 


            <dependency>
            <groupId>nz.co.nimbal</groupId>
            <artifactId>auto-platform</artifactId>
            <version>1.8.4</version>
            <type>test-jar</type>
            <scope>test</scope>
            </dependency>

Run the test by updating your test tag (e.g  tags = {"dailysession"}) in TestRunner.java and command mvn install. 

See the detailed reports in target/generated-report folder. 
See the txt summary reports in target/failesafe-report folder

